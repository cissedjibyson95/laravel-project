@extends('layouts/base')



@section('content')

        <img src="{{ asset('/images/senegal.png') }}" width='100px' height="100px" alt="logo" class="my-12 rounded-full shadow-md">
        <h1 class='mt-5 text-5xl font-semibold text-indigo-600'>Hello from Senegal</h1>
        <p class='text-lg text-gray-800'>Il est actuellement {{date('H:i')}}</p>

@endsection