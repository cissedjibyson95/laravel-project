@extends('layouts/base', ['title' => 'About Us | '])


@section('content')

        <img src="{{ asset('/images/logo-jc-wedev.png') }}" width='100px' height="100px" alt="senegal" class="mt-12 rounded shadow-md h-32">
        <h2 class="mb-5 text-gray-700">Built with <span class='text-pink-500'>&hearts;</span> by LES TEACHERS DU NET.</h2>
        <p><a href="{{ route('home') }}" class='text-indigo-500 hover:text-indigo-600 underline'>Revenir vers la page d'accueil</a></p>

@endsection
